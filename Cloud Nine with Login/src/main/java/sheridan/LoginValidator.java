package sheridan;

public class LoginValidator {

	private static final String LOGIN_NAME_REGEX = "^(?![0-9])([A-Za-z0-9]{6,})$";
	
	public static boolean isValidLoginName( String loginName ) {
		boolean isValid = false;
		if (loginName != null) {
			isValid = loginName.matches(LOGIN_NAME_REGEX);
		}
		return isValid;
	}
}
