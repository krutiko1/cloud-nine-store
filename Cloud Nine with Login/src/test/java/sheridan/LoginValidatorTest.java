package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginCharactersRegular( ) {
		assertTrue("Valid login was considered invalid" , LoginValidator.isValidLoginName( "MrRamses1" ) );
	}
	
	@Test
	public void testIsValidLoginCharactersBoundaryIn( ) {
		assertTrue("Valid login was considered invalid" , LoginValidator.isValidLoginName( "R1amses123" ) );
	}
	
	@Test
	public void testIsValidLoginCharactersBoundaryOut( ) {
		assertFalse("Invalid login was considered valid" , LoginValidator.isValidLoginName( "1Rames123" ) );
	}
	
	@Test
	public void testIsValidLoginCharactersException( ) {
		assertFalse("Invalid login was considered valid" , LoginValidator.isValidLoginName( "12345." ) );
	}
	
	@Test
	public void testIsValidLoginLengthRegular( ) {
		assertTrue("Valid login was considered invalid" , LoginValidator.isValidLoginName( "mrramses" ) );
	}
	
	@Test
	public void testIsValidLoginLengthBoundaryIn () {
		assertTrue("Valid login was considered invalid" , LoginValidator.isValidLoginName( "ramses" ) );
	}
	
	@Test
	public void testIsValidLoginLengthBoundaryOut( ) {
		assertFalse("Invalid login was considered valid" , LoginValidator.isValidLoginName( "ramse" ) );
	}
	
	@Test
	public void testIsValidLoginLengthException( ) {
		assertFalse("Invalid login was considered valid" , LoginValidator.isValidLoginName( null ) );
	}
	
}
